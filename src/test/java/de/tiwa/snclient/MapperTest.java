package de.tiwa.snclient;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class MapperTest {

    @Test
    public void mapToTag() throws IOException {
        // before
        String tagTitle = "tagTitle";
        String uuid = "uuid";
        Tag tag = new Tag();
        tag.title = tagTitle;
        tag.referenceNoteUuid = Lists.newArrayList(uuid);

        // execute
        Item item = TagMapper.map(tag);
        Tag back = TagMapper.map(item);

        // verify
        Assert.assertEquals(back.title, tagTitle);
        Assert.assertNotNull(back.uuid);
        Assert.assertEquals(uuid, back.referenceNoteUuid.get(0));
    }

    @Test
    public void mapToNote() throws IOException {
        // before
        String noteTitle = "noteTitle";
        Note note = new Note();
        note.title = noteTitle;

        // execute
        Item item = NoteMapper.map(note);
        Note back = NoteMapper.map(item);

        // verify
        Assert.assertEquals(back.title, noteTitle);
        Assert.assertNotNull(back.uuid);
    }
}
