package de.tiwa.snclient;

public interface StandardNotesClient {

    ClientSettings getSettings();

    ResponseContainer syncItems(RequestContainer requestContainer);
}
