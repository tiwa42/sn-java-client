package de.tiwa.snclient;

public class StandardNotesClientImpl implements StandardNotesClient {

    private final Client client;

    public StandardNotesClientImpl(Client client) {
        this.client = client;
    }

    @Override
    public ClientSettings getSettings() {
        return client.getClientSettings();
    }

    @Override
    public ResponseContainer syncItems(RequestContainer requestContainer) {
        if (requestContainer == null) {
            requestContainer = new RequestContainer();
        }
        return client.syncNotes(requestContainer);
    }
}
