# Standard Notes Java Client
## Description
This Java Libary communicate with a Standard Notes Backend. So you don't have to worry about HTTP and Encryption. 

## Include
Maven
```
<dependency>
	<groupId>de.tiwa</groupId>
	<artifactId>snclient</artifactId>
	<version>0.2.0</version>
	<type>pom</type>
</dependency>
```
Gradle
```
implementation 'de.tiwa:snclient:0.2.0'
```

## How-to-use

Get the Client

```
StandardNotesClient standardNotesClient = StandardNotesClientFactory.createStandardNotesClient(mail, pw);
```



Get all Items in the Account. An Item could be type of Note, Tag, Component ... 

```
RequestContainer requestContainer = new RequestContainer();
ResponseContainer responseContainer = standardNotesClient.syncItems(requestContainer);
```

Every item has a a content_type field. It could be type of Note, Tag, Component ... 
There is also decryptet_content field. This contains the content as JSON. For content_type Tag and Note you can Map the JSON with this Libary 

```
List<Tag> tagList = new ArrayList();
List<Note> noteList = new ArrayList();
for (Item item : responseContainer.retrieved_items) {
      if (item.content_type.equals(TagMapper.CONTENT_TYPE)) {
          tagList.add(TagMapper.map(item));
      }
      if (item.content_type.equals(NoteMapper.CONTENT_TYPE)) {
          noteList.add(NoteMapper.map(item));
      }
```

Create a new Note
```
List<Item> items = new ArrayList();
Note note = new Note();
note.text = "content";
note.title = "title";
items.add(NoteMapper.map(note));
RequestContainer requestContainer = new RequestContainer();
requestContainer.items = items;
standardNotesClient.syncItems(requestContainer);
```

## License
GNU Affero General Public License v3.0. 